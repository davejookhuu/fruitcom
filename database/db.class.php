<?php

/**
 * Description of FruitComDB
 * Template DB Class
 * @author Dave Jookhuu
 */
class DB {

    private $conn;
    private $host;
    private $user;
    private $password;
    private $baseName;
    private $port;
    private $Debug;
    private $NumRows;
    private $BadQuery;
    
    function __construct($params = array()) {
        $this->conn = false;
        $this->host = 'localhost'; //hostname
        $this->user = 'root'; //username
        $this->password = 'admin'; //password
        $this->baseName = 'fruitcom'; //name of your database
        $this->port = '3306';
        $this->debug = true;
        $this->connect();
    }

    function __destruct() {
        $this->disconnect();
    }

    function connect() {
        if (!$this->conn) {
            $this->conn = mysqli_connect($this->host, $this->user, $this->password);
            mysqli_select_db($this->conn, $this->baseName);
            mysqli_set_charset($this->conn, 'utf8');

            if (!$this->conn) {
                $this->status_fatal = true;
                echo "<script>console.log( 'Connection failed!')</script>"; 
                die();
            } else {
                $this->status_fatal = false;
                echo "<script>console.log( 'Connection success!')</script>";  
            }
        }

        return $this->conn;
    }

    function disconnect() {
        if ($this->conn) {
            @pg_close($this->conn);
        }
    }

    function getOne($query) { // getOne function: when you need to select only 1 line in the database
        $cnx = $this->conn;
        if (!$cnx || $this->status_fatal) {
            echo 'GetOne -> Connection failed';
            die();
        }

        $cur = @mysqli_query($cnx, $query);

        if ($cur == FALSE) {
            $errorMessage = @pg_last_error($cnx);
            $this->handleError($query, $errorMessage);
        } else {
            $this->Error = FALSE;
            $this->BadQuery = "";
            $tmp = mysqli_fetch_array($cur, MYSQLI_ASSOC);

            $return = $tmp;
        }

        @mysqli_free_result($cur);
        return $return;
    }

    function getAll($query) { // getAll function: when you need to select more than 1 line in the database
        $cnx = $this->conn;
        if (!$cnx || $this->status_fatal) {
            echo 'GetAll -> Connection failed';
            die();
        }

        mysqli_query($cnx, "SET NAMES 'utf8'");
        $cur = mysqli_query($cnx, $query);
        $return = array();

        while ($data = mysqli_fetch_assoc($cur)) {
            array_push($return, $data);
        }

        return $return;
    }

    function execute($query, $use_slave = false) { // execute function: to use INSERT or UPDATE
        $cnx = $this->conn;
        if (!$cnx || $this->status_fatal) {
            return null;
        }

        $cur = @mysqli_query($cnx, $query);

        if ($cur == FALSE) {
            $ErrorMessage = @mysqli_last_error($cnx);
            $this->handleError($query, $ErrorMessage);
        } else {
            $this->Error = FALSE;
            $this->BadQuery = "";
            $this->NumRows = mysqli_affected_rows($cnx);
            echo "<script> console.log('TESTING: ". $this->NumRows ." effected!');</script>";
            return;
            
        }
        @mysqli_free_result($cur);
    }

    function handleError($query, $str_error) {
        $this->Error = TRUE;
        $this->BadQuery = $query;
        if ($this->Debug) {
            echo "Query : " . $query . "<br>";
            echo "Error : " . $str_error . "<br>";
        }
    }
    
     public function __toString() {
        echo "<script>console.log('toString() was called Dave!')</script>";
    }

}
