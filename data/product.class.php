<?php
include_once "database/db.class.php";
/**
 * Description of product
 *
 * @author 523989
 */
class Product {
    //put your code here
    private $prodId;
    private $prodName;
    private $prodPrice;
    private $prodQty;
    private $vendorId;
    
    function getProdId() {
        return $this->prodId;
    }

    function getProdName() {
        return $this->prodName;
    }

    function getProdPrice() {
        return $this->prodPrice;
    }

    function getProdQty() {
        return $this->prodQty;
    }

    function getVendorId() {
        return $this->vendorId;
    }

    function setProdId($prodId) {
        $this->prodId = $prodId;
    }

    function setProdName($prodName) {
        $this->prodName = $prodName;
    }

    function setProdPrice($prodPrice) {
        $this->prodPrice = $prodPrice;
    }

    function setProdQty($prodQty) {
        $this->prodQty = $prodQty;
    }

    function setVendorId($vendorId) {
        $this->vendorId = $vendorId;
    }

    function __construct($prodId, $prodName, $prodPrice, $prodQty, $vendorId) {
        $this->prodId = $prodId;
        $this->prodName = $prodName;
        $this->prodPrice = $prodPrice;
        $this->prodQty = $prodQty;
        $this->vendorId = $vendorId;
    }

    function addProduct()
    {    
        $db = new DB(); 
        $prodName = $db->getOne("SELECT prodName FROM productavailable WHERE prodName = '". strtolower($this->getProdName()) ."' AND vendorId=". $this->vendorId);
        if ($prodName["prodName"] === strtolower($this->getProdName()))
        {
            header("Location: vend-admin.php?productExists");            
        }
        else
        {
           $db->execute("INSERT INTO productavailable (prodName, prodPrice, prodQty, vendorId) VALUES ('". 
                strtolower(mysqli_real_escape_string($db->connect(), $this->getProdName())) ."', '". 
                strtolower(mysqli_real_escape_string($db->connect(), $this->getProdPrice())) ."', '". 
                strtolower(mysqli_real_escape_string($db->connect(), $this->getProdQty())) ."', '". 
                strtolower(mysqli_real_escape_string($db->connect(), $this->getVendorId())) ."')");
            
            // Redirect to registration page
            header("Location: vend-admin.php?insertSuccess");
            // Print "User Exists" message
        }
    }
    
    function updateProduct()
    {
        //session_start();
        $db = new DB();
        //$updatedTime = $db->getOne("SELECT dateTime FROM productavailable WHERE id=" . $this->getProdId());
        //echo $updatedTime['dateTime'];
        
        $db->execute("UPDATE productavailable SET prodName = '" . $this->getProdName(). "', prodPrice = '" . $this->getProdPrice(). "', prodQty = '" . $this->getProdQty() . "', vendorId = '" . $this->getVendorId() . "'  WHERE id=" . $this->getProdId());
        $_SESSION["lastUpdated"] = time();
        header("Location: vend-admin.php?insertSuccess");    
    }
    
    function deleteProduct()
    {
        $db = new DB(); 
        if($db->execute("DELETE FROM productavailable WHERE prodId = '". $this->getProdId() ."'"))
            header("Location: vend-admin.php?insertSuccess");
        else
            header("Location: vend-admin.php?insertFail"); 
       
    }
}
