<?php
/**
 * Description of Customer
 *
 * @author Dave Jookhuu
 */
class Customer {
    
    // Setting up properties
    private $id;
    private $customerFirstName;
    private $customerLastName;    
    private $customerEmail;
    private $customerPhone;
    private $customerPass;

    // Setting up Getters & Setters    
    function getCustomerFirstName() {
        return $this->customerFirstName;
    }

    function getCustomerLastName() {
        return $this->customerLastName;
    }

    function getCustomerEmail() {
        return $this->customerEmail;
    }

    function getCustomerPhone() {
        return $this->customerPhone;
    }

    function getCustomerPass() {
        return $this->customerPass;
    }

    function setCustomerFirstName($customerFirstName) {
        $this->customerFirstName = $customerFirstName;
    }

    function setCustomerLastName($customerLastName) {
        $this->customerLastName = $customerLastName;
    }

    function setCustomerEmail($customerEmail) {
        $this->customerEmail = $customerEmail;
    }

    function setCustomerPhone($customerPhone) {
        $this->customerPhone = $customerPhone;
    }

    function setCustomerPass($customerPass) {
        $this->customerPass = $customerPass;
    }
    
    function __construct($customerFirstName, $customerLastName, $customerEmail, $customerPhone, $customerPass) {
        $this->customerFirstName = $customerFirstName;
        $this->customerLastName = $customerLastName;
        $this->customerEmail = $customerEmail;
        $this->customerPhone = $customerPhone;
        $this->customerPass = $customerPass;
    }   
    
    public function __toString() {
        echo "<script>console.log( 'This test for ". $this->customerFirstName ." ". $this->customerLastName ."')</script>";
    }       
}
