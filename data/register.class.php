<?php
include_once "database/db.class.php";
include_once "customer.class.php";
include_once "vendor.class.php";
include_once "login.class.php";    
/**
 * Description of register
 *
 * @author 523989
 */
class Register {
    
    
    function registerBuyer($buyerFirstName, $buyerLastName, $buyerEmail, $buyerPhone, $buyerPass)
    {
        session_start();
        $db = new DB();
        $customer = new Customer(
                mysqli_real_escape_string($db->connect(), strtolower(trim($buyerFirstName))), 
                mysqli_real_escape_string($db->connect(), strtolower(trim($buyerLastName))), 
                mysqli_real_escape_string($db->connect(), strtolower(trim($buyerEmail))), 
                mysqli_real_escape_string($db->connect(), strtolower(trim($buyerPhone))), 
                mysqli_real_escape_string($db->connect(), trim($buyerPass)));        

        
        $user = $db->getOne("SELECT custEmail FROM customer WHERE custEmail = '". $customer->getCustomerEmail() ."'");
        if ($user["custEmail"] === $customer->getCustomerEmail())
        {
            // User exists
            $_SESSION["buyerExists"] = "Buyer Exists"; 
            // For testing
            echo "<script>console.log( 'The user ". $customer->getCustomerEmail(). " exists!')</script>";            
            // Redirect to registration page
            header("Location: registration.php?active=buyer");
            // Print "User Exists" message
        }
        else
        {
            // Register
            $hashed_pass = password_hash($customer->getCustomerPass(), PASSWORD_DEFAULT);
            if($db->execute("INSERT INTO customer (custFirstName, custLastName, custEmail, custPhone, custPass) VALUES ('". $customer->getCustomerFirstName() ."', '". $customer->getCustomerLastName() ."', '". $customer->getCustomerEmail() ."', '". $customer->getCustomerPhone() ."', '". $hashed_pass ."')"))
            {
                // For testing
                echo "<script>console.log( 'New customer was added!')</script>";
            }
            else
            {
                // For testing
                echo "<script>console.log( 'Error adding new customer!' )</script>";
            }
            
            // For Testing
            echo "<script>console.log( 'The user ". $customer->getCustomerEmail() . " does NOT exist!')</script>";
            header("Location: login.php");
        }        
    }
    
    function registerSeller($sellerName, $sellerEmail, $sellerPhone, $sellerPass)
    {
        session_start();
        $db = new DB();
        $vendor = new Vendor(
                mysqli_real_escape_string($db->connect(), strtolower(trim($sellerName))),
                mysqli_real_escape_string($db->connect(), strtolower(trim($sellerEmail))),
                mysqli_real_escape_string($db->connect(), strtolower(trim($sellerPhone))),
                mysqli_real_escape_string($db->connect(), trim($sellerPass)));
       
        $user = $db->getOne("SELECT vendorEmail FROM vendor WHERE vendorEmail = '". $vendor->getVendorEmail() ."'");
        if ($user["vendorEmail"] === $vendor->getVendorEmail())
        {
            // User exists
            $_SESSION["sellerExists"] = "Seller Exists"; 
            // For testing
            echo "<script>console.log( 'The user ". $vendor->getVendorEmail() . " exists!')</script>";
             
            // Redirect to registration page
            header("Location: registration.php?active=seller");
            
            // Print "User Exists" message
        }
        else
        {
            // Register
            $hashed_pass = password_hash($vendor->getVendorPass(), PASSWORD_DEFAULT);
            if($db->execute("INSERT INTO vendor (vendorName, vendorEmail, vendorPhone, vendorPass) VALUES ('". $vendor->getVendorName() ."', '". $vendor->getVendorEmail() ."', '". $vendor->getVendorPhone() ."', '". $hashed_pass ."')"))
            {
                echo "<script>console.log( 'New customer was added!')</script>";
            }
            else
            {
                echo "<script>console.log( 'Error adding new customer!' )</script>";
            }
            
            echo "<script>console.log( 'The user ". $vendor->getVendorEmail() . " does NOT exist!')</script>";
            header("Location: login.php");
        }
        
    }   
    
}
