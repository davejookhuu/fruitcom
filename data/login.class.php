<?php
include_once "database/db.class.php";

/**
 * Description of login
 *
 * @author Dave Jookhuu
 */
class Login {    
    
    private $userEmail;
    private $userPass;
    private $accountType;
        
    // Checking if user exists
    public function userLogin($accountType, $email, $pass) 
    {   
        session_start();
        $db = new DB();        
        $this->accountType = mysqli_real_escape_string($db->connect(), $accountType);
        $this->userEmail = mysqli_real_escape_string($db->connect(), $email);
        $this->userPass = mysqli_real_escape_string($db->connect(), $pass);        
              
        $user;  
        if($this->accountType === "customer")
        {
            
            $user = $db->getOne("SELECT custEmail, custPass FROM customer WHERE custEmail = '". $this->userEmail ."'");            
            if ($user['custEmail']  === $this->userEmail && password_verify($this->userPass, $user['custPass']))
            {   
                $_SESSION["loggedUser"] = $user['custEmail'];
                $_SESSION["customerType"] = "customer";
                
                // For testing
                echo "<script>console.log( 'User Session: ". $_SESSION["loggedUser"] ." esits!')</script>";
                header("Location: cust-admin.php");               
            }   
            else
            {
                $_SESSION["invalidUser"] = "Incorect Usernamr or Password";
                $this->backToLogin();
            }
            echo $accountType;
        }
        elseif($accountType === "vendor")
        {
            $user = $db->getOne("SELECT * FROM vendor WHERE vendorEmail = '". $this->userEmail ."'");
            if ($user['vendorEmail']  === $this->userEmail && password_verify($this->userPass, $user['vendorPass']))
            {   
                $_SESSION["loggedUser"] = $user['vendorEmail'];
                $_SESSION["customerType"] = "vendor";
                $_SESSION["loggedInVendorId"] = $user['id'];
                // For testing
                echo "<script>console.log( 'User Session: ". $_SESSION["loggedUser"] ." esits!')</script>";                
                header("Location: vend-admin.php");               
            }
            else
            {
                $_SESSION["invalidUser"] = "Incorect Usernamr or Password";
                $this->backToLogin();
            }            
            echo $accountType;
        }        
    }
    
    public function backToLogin()
    {       
        $_SESSION["errorMessage"] = "Username does not exist!";        
        echo "<script>console.log( 'User ". $this->userEmail ." does not esit!')</script>";
        header("Location: login.php");
        return false;        
    }
    
    public function logOut()
    {        
        unset($_SESSION["loggedUser"]);
        $_SESSION["loggedUser"] = "";
        session_destroy();
        header("Location: login.php");
    }
    
}