<?php
/**
 * Description of Vendor
 * @author Dave Jookhuu
 */

class Vendor{  
    
    // Setting up properties
    private $id;
    private $vendorName;
    private $vendorEmail;
    private $vendorPhone;
    private $vendorPass;   
    
   
    // Setting up Getters & Setters
    function getVendorName() {
        return $this->vendorName;
    }

    function getVendorEmail() {
        return $this->vendorEmail;
    }

    function getVendorPhone() {
        return $this->vendorPhone;
    }

    function getVendorPass() {
        return $this->vendorPass;
    }

    function setVendorName($vendorName) {
        $this->vendorName = $vendorName;
    }

    function setVendorEmail($vendorEmail) {
        $this->vendorEmail = $vendorEmail;
    }

    function setVendorPhone($vendorPhone) {
        $this->vendorPhone = $vendorPhone;
    }

    function setVendorPass($vendorPass) {
        $this->vendorPass = $vendorPass;
    }
    
    // Setting constructor 
    function __construct($vendorName, $vendorEmail, $vendorPhone, $vendorPass) {
        $this->vendorName = $vendorName;
        $this->vendorEmail = $vendorEmail;
        $this->vendorPhone = $vendorPhone;
        $this->vendorPass = $vendorPass;
    }
    
    // ToString Method for testing...
    public function __toString() {
        echo "<script>console.log( 'This test for ". $this->vendorName ."')</script>";
    }


}
