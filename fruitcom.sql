-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2017 at 04:35 PM
-- Server version: 5.7.19-log
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fruitcom`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientorder`
--

CREATE TABLE `clientorder` (
  `id` int(11) NOT NULL,
  `prodName` varchar(30) NOT NULL,
  `prodPrice` double NOT NULL,
  `prodQty` int(11) NOT NULL,
  `totalPrice` double NOT NULL,
  `vendorName` varchar(50) NOT NULL,
  `vendorId` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `custId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clientorder`
--

INSERT INTO `clientorder` (`id`, `prodName`, `prodPrice`, `prodQty`, `totalPrice`, `vendorName`, `vendorId`, `purchaseDate`, `custId`) VALUES
(1, 'appale', 5.99, 100, 599, 'Saks Berries', 1, '2017-08-20', 71),
(2, 'banana', 1.2, 200, 240, 'Farmer Market', 2, '2017-09-09', 71),
(3, 'banana', 1.2, 200, 240, 'Farmer Market', 2, '2017-09-09', 1),
(4, 'appale', 5.99, 100, 599, 'Saks Berries', 1, '2017-08-20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `custFirstName` varchar(30) NOT NULL,
  `custLastName` varchar(30) NOT NULL,
  `custEmail` varchar(50) NOT NULL,
  `custPhone` varchar(14) NOT NULL,
  `custPass` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `custFirstName`, `custLastName`, `custEmail`, `custPhone`, `custPass`) VALUES
(1, 'Dave', 'Jookhuu', 'info@davejookhuu.com', '(403)616-3240', '$2y$10$hPszXIr9bHXsKXZr8DQQE.vkknGJqQ.YHmRcqUBd8MbV7tXgUyRRW'),
(2, 'John', 'Doe', 'abc@gmail.com', '(403)999-6363', '$2y$10$hPszXIr9bHXsKXZr8DQQE.vkknGJqQ.YHmRcqUBd8MbV7tXgUyRRW'),
(71, 'Marla', 'Davaa', 'marla@gmail.com', '4036163240', '$2y$10$hPszXIr9bHXsKXZr8DQQE.vkknGJqQ.YHmRcqUBd8MbV7tXgUyRRW'),
(72, 'dave', 'Joo', 'abca@gmail.com', '4036163240', '$2y$10$hPszXIr9bHXsKXZr8DQQE.vkknGJqQ.YHmRcqUBd8MbV7tXgUyRRW'),
(74, 'dave', 'Joo', 'max@max.com', '4036163240', '$2y$10$THkI9sZ24NJd5t1.CUuUC.A9YO.mROhbY8FihhCNCzYO6TrD5LNse'),
(75, 'daagii', 'jookhuuu', 'newuser@davejookhuu.com', '4036163240', '$2y$10$7dor3EugHVJr1FJnEm5UwOHc1Y0RollAmjJPcJr7hy7Z68C37mRO2');

-- --------------------------------------------------------

--
-- Table structure for table `productavailable`
--

CREATE TABLE `productavailable` (
  `id` int(11) NOT NULL,
  `prodName` varchar(30) NOT NULL,
  `prodPrice` double NOT NULL,
  `prodQty` int(11) NOT NULL,
  `vendorId` int(11) NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productavailable`
--

INSERT INTO `productavailable` (`id`, `prodName`, `prodPrice`, `prodQty`, `vendorId`, `dateTime`) VALUES
(1, 'banana', 1.2, 500, 1, '2017-09-19 02:41:06'),
(2, 'apple', 4.99, 200, 2, '2017-09-19 02:41:06'),
(3, 'apple', 3.99, 50, 10, '2017-09-19 02:41:06'),
(6, 'banana', 1.3, 75, 10, '2017-09-19 02:41:06'),
(7, 'pineapple', 7.5, 25, 10, '2017-09-19 02:41:06'),
(8, 'pineapple', 8.12, 51, 14, '2017-09-19 02:41:06'),
(10, 'straberry', 7.99, 56, 1, '2017-09-19 02:41:06'),
(13, 'grapes', 3.99, 45, 14, '2017-09-19 02:41:06'),
(15, 'strabbery', 3.99, 60, 14, '2017-09-19 04:42:26'),
(17, 'watermelon', 10.99, 100, 14, '2017-09-19 02:41:06'),
(18, 'apple', 3.99, 45, 14, '2017-09-19 02:41:06'),
(20, 'kiwi', 5.66, 70, 14, '2017-09-19 02:41:06'),
(21, 'cranberries', 2.99, 300, 14, '2017-09-19 02:41:06'),
(22, 'test', 10.99, 100, 14, '2017-09-19 02:41:38'),
(23, 'strabbery', 3.99, 500, 15, '2017-09-19 06:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `productsold`
--

CREATE TABLE `productsold` (
  `id` int(11) NOT NULL,
  `custId` int(11) NOT NULL,
  `prodName` varchar(30) NOT NULL,
  `prodPrice` double NOT NULL,
  `prodQty` int(11) NOT NULL,
  `soldDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productsold`
--

INSERT INTO `productsold` (`id`, `custId`, `prodName`, `prodPrice`, `prodQty`, `soldDate`) VALUES
(1, 1, 'apple', 4.99, 10, '2017-09-10'),
(2, 2, 'banana', 1.2, 20, '2017-09-01');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `name`) VALUES
(11, 'Pineapple'),
(12, '7.5'),
(13, '7.50'),
(14, 'Alberta Berries'),
(15, '1'),
(16, '10'),
(17, '2017-09-18'),
(18, 'Buy Now'),
(19, 'Pineapple'),
(20, '7.5'),
(21, '7.50'),
(22, 'Alberta Berries'),
(23, '1'),
(24, '10'),
(25, '2017-09-18'),
(26, 'Buy Now'),
(27, 'Pineapple'),
(28, '7.5'),
(29, '7.50'),
(30, 'Alberta Berries'),
(31, '1'),
(32, '10'),
(33, '2017-09-18'),
(34, 'Buy Now'),
(35, 'Pineapple'),
(36, '7.5'),
(37, '7.50'),
(38, 'Alberta Berries'),
(39, '1'),
(40, '10'),
(41, '2017-09-18'),
(42, 'Buy Now'),
(43, 'Pineapple'),
(44, '7.5'),
(45, '7.50'),
(46, 'Alberta Berries'),
(47, '1'),
(48, '10'),
(49, '2017-09-18'),
(50, 'Buy Now'),
(51, 'Pineapple'),
(52, '7.5'),
(53, '15.00'),
(54, 'Alberta Berries'),
(55, '2'),
(56, '10'),
(57, '2017-09-18'),
(58, 'Buy Now'),
(59, 'Pineapple'),
(60, '7.5'),
(61, '15.00'),
(62, 'Alberta Berries'),
(63, '2'),
(64, '10'),
(65, '2017-09-18'),
(66, 'Buy Now'),
(67, 'Pineapple'),
(68, '7.5'),
(69, '15.00'),
(70, 'Alberta Berries'),
(71, '2'),
(72, '10'),
(73, '2017-09-18'),
(74, 'Buy Now'),
(75, 'Pineapple'),
(76, '7.5'),
(77, '15.00'),
(78, 'Alberta Berries'),
(79, '2'),
(80, '10'),
(81, '2017-09-18'),
(82, 'Buy Now'),
(83, 'Pineapple'),
(84, '7.5'),
(85, '15.00'),
(86, 'Alberta Berries'),
(87, '2'),
(88, '10'),
(89, '2017-09-18'),
(90, 'Buy Now'),
(91, 'Strabbery'),
(92, '4.88'),
(93, '4.88'),
(94, 'Alberta Berries'),
(95, '1'),
(96, '10'),
(97, '2017-09-18'),
(98, 'Buy Now'),
(99, 'Strabbery'),
(100, '4.88'),
(101, '9.76'),
(102, 'Alberta Berries'),
(103, '2'),
(104, '10'),
(105, '2017-09-18'),
(106, 'Buy Now'),
(107, 'Strabbery'),
(108, '4.88'),
(109, '9.76'),
(110, 'Alberta Berries'),
(111, '2'),
(112, '10'),
(113, '2017-09-18'),
(114, 'Buy Now'),
(115, 'Strabbery'),
(116, '4.88'),
(117, '9.76'),
(118, 'Alberta Berries'),
(119, '2'),
(120, '10'),
(121, '2017-09-18'),
(122, 'Buy Now'),
(123, 'Strabbery'),
(124, '4.88'),
(125, '9.76'),
(126, 'Alberta Berries'),
(127, '2'),
(128, '10'),
(129, '2017-09-18'),
(130, 'Buy Now'),
(131, 'Strabbery'),
(132, '4.88'),
(133, '9.76'),
(134, 'Alberta Berries'),
(135, '2'),
(136, '10'),
(137, '2017-09-18'),
(138, 'Buy Now'),
(139, 'Apple'),
(140, '3.79'),
(141, '3.79'),
(142, 'Farmer Market'),
(143, '1'),
(144, '2'),
(145, '2017-09-18'),
(146, 'Banana'),
(147, '1.19'),
(148, '1.19'),
(149, 'Sask Berry'),
(150, '1'),
(151, '1'),
(152, '2017-09-18'),
(153, 'Pineapple'),
(154, '7.5'),
(155, '7.50'),
(156, 'Alberta Berries'),
(157, '1'),
(158, '10'),
(159, '2017-09-18'),
(160, 'Strabbery'),
(161, '4.88'),
(162, '4.88'),
(163, 'Alberta Berries'),
(164, '1'),
(165, '10'),
(166, '2017-09-18'),
(167, 'Buy Now'),
(168, 'Apple'),
(169, '3.79'),
(170, '11.37'),
(171, 'Farmer Market'),
(172, '3'),
(173, '2'),
(174, '2017-09-18'),
(175, 'Banana'),
(176, '1.19'),
(177, '3.57'),
(178, 'Sask Berry'),
(179, '3'),
(180, '1'),
(181, '2017-09-18'),
(182, 'Pineapple'),
(183, '7.5'),
(184, '30.00'),
(185, 'Alberta Berries'),
(186, '4'),
(187, '10'),
(188, '2017-09-18'),
(189, 'Strabbery'),
(190, '4.88'),
(191, '24.40'),
(192, 'Alberta Berries'),
(193, '5'),
(194, '10'),
(195, '2017-09-18'),
(196, 'Buy Now');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `custId` int(11) NOT NULL,
  `vendorId` int(11) NOT NULL,
  `transdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `custId`, `vendorId`, `transdate`) VALUES
(1, 1, 1, '2017-09-12'),
(2, 2, 2, '2017-09-13');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `vendorName` varchar(30) NOT NULL,
  `vendorEmail` varchar(50) NOT NULL,
  `vendorPhone` varchar(30) NOT NULL,
  `vendorPass` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `vendorName`, `vendorEmail`, `vendorPhone`, `vendorPass`) VALUES
(1, 'Sask Berry', 'saskberry@gmail.com', '(403)827-5370', '$2y$10$hPszXIr9bHXsKXZr8DQQE.vkknGJqQ.YHmRcqUBd8MbV7tXgUyRRW'),
(2, 'Farmer Market', 'farmermarket@gmail.com', '(403)6214-2589', '$2y$10$hPszXIr9bHXsKXZr8DQQE.vkknGJqQ.YHmRcqUBd8MbV7tXgUyRRW'),
(10, 'Alberta Berries', 'newvendor@alberta.com', '(403)999-9999', '$2y$10$hPszXIr9bHXsKXZr8DQQE.vkknGJqQ.YHmRcqUBd8MbV7tXgUyRRW'),
(14, 'Manitoba Berries', 'max@max.com', '(403)999-9999', '$2y$10$rt/ux0EB.b/bWTR2/qRKNur8ffmGWSrS5fGJ9PGNnUDzJvzraeW1O'),
(15, 'davaaseren', 'newuser@davejookhuu.com', '4036163244', '$2y$10$iervS8FJLW0789Ut7lzwo.iyq8cZ52HuBQpH/hyAxaKTn/DkzXVO.');

-- --------------------------------------------------------

--
-- Table structure for table `vendortransaction`
--

CREATE TABLE `vendortransaction` (
  `id` int(11) NOT NULL,
  `prodName` varchar(30) NOT NULL,
  `prodPrice` double NOT NULL,
  `prodQty` int(11) NOT NULL,
  `totalPrice` double NOT NULL,
  `transactiondate` date NOT NULL,
  `clientId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientorder`
--
ALTER TABLE `clientorder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productavailable`
--
ALTER TABLE `productavailable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productsold`
--
ALTER TABLE `productsold`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custId` (`custId`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custId` (`custId`),
  ADD KEY `vendorId` (`vendorId`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendortransaction`
--
ALTER TABLE `vendortransaction`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientorder`
--
ALTER TABLE `clientorder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `productavailable`
--
ALTER TABLE `productavailable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `productsold`
--
ALTER TABLE `productsold`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `vendortransaction`
--
ALTER TABLE `vendortransaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `productsold`
--
ALTER TABLE `productsold`
  ADD CONSTRAINT `productsold_ibfk_1` FOREIGN KEY (`custId`) REFERENCES `customer` (`id`);

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`custId`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `transaction_ibfk_2` FOREIGN KEY (`vendorId`) REFERENCES `vendor` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
