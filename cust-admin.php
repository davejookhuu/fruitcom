<?php
session_start();
include_once "/database/db.class.php";
include_once "/data/customer.class.php";
include_once "/data/login.class.php";    
include_once "header.php";

$logOut = new Login();
$db = new DB();
echo "<div class='container'>";
if(isset($_SESSION["loggedUser"]) && !empty($_SESSION["loggedUser"]))
{
    if($_SESSION["customerType"] === "customer")
    {
        echo "<h6>Welcome <strong>". $_SESSION["loggedUser"]. "</strong>!</h6>";
        if(isset($_REQUEST["logOut"]))
        {
            $logOut->logOut();
        }
    }
    else
    {
        header("Location: vend-admin.php");
    }
    
}
else    
{
    $logOut->logOut();
}

if(isset($_REQUEST["buyProduct"]))
{
   if (isset($_POST))
   {
      //`prodName`, `prodPrice`, `prodQty`, `totalPrice`, `vendorName`, `vendorId`, `purchaseDate`
      // $insertQuery = "INSERT INTO test (prodName, prodPrice, totalPrice, vendorName, prodQty, vendorId, purchaseDate) VALUES ('";
      $arr = [];
      $arr2 = [];
      $counter = 0;
      foreach ($_POST as $name=>$value)
      {
        if($name != "buyProduct")
            echo $name." is"." ".$value."<br/>";
        array_push($arr, array($name => $value));
            $counter++;
            
      }
      //echo $counter;
      //$insertQuery. "')";
      //$db->execute($insertQuery);
      //print_r(json_encode($arr1));
   }
}

?>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
    <input class="btn btn-info" type="submit" name="logOut" value="Logout" />
</form>

  <h2>Availabe Products</h2>
  <p>Lorem ipsum dolor sit amet, odio dicta mauris in commodo, tellus ut leo est, hac viverra ac amet ridiculus, a sed egestas donec vitae fusce, risus pede id ut non. Odio phasellus ac est, facilisis nec labore praesent, vitae semper urna, id venenatis ipsum leo felis nam orci. Curabitur egestas suscipit enim mauris morbi, consectetuer et justo lorem, at nulla sed bibendum in vitae. Mollis elit, quis mauris. Cras rerum mattis ligula mus, vestibulum dolor lectus, aliquam lorem enim, duis ut nibh enim cursus. Nam habitasse, natus vel nisl quis vel quam.</p>            
  <form action="" method="POST">  
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Product Price</th>
        <th>Product Quantity</th>
        <th>Total Price</th>
        <th>Vendor Name</th>
        <th>Vendor Email</th>
        <th>Vendor Contact</th>
        <th>Buy Quantity</th>        
      </tr>
    </thead>
    <tbody>
         
<?php 

$productsQuery = "SELECT productavailable.prodName, MIN(productavailable.prodPrice) AS prodPrice, productavailable.prodQty, vendor.vendorName,
vendor.id AS vedorId, vendor.vendorEmail, vendor.vendorPhone
FROM productavailable 
LEFT JOIN vendor
ON productavailable.vendorId=vendor.id
GROUP BY productavailable.prodName
ASC";


$products = $db->getAll($productsQuery); // select ALL from users		
$numberOfProducts = count($products); // return the number of lines


echo $numberOfProducts.' products available to buy.<br />';
$counter = 1;
foreach($products as $product) { // display the list
    
    echo "<tr>";
    echo "<td><input type='text' value='".ucwords($product['prodName'])."' name='prodName_$counter' readonly/></td>";
    echo "<td><input type='text' value='".$product['prodPrice']."' name='prodPrice_$counter' class='prodPrice' readonly /></td>";
    echo "<td>".$product['prodQty']."</td>";
    echo "<td><input type='text' value='' name='totalPrice_$counter' class='totalPrice' readonly />";
    echo "<td><input type='text' value='".ucwords($product['vendorName'])."' name='vendorName_$counter' readonly /></td>";
    echo "<td><a href='mailto:".$product['vendorEmail']."'>".$product['vendorEmail']."</a></td>";
    echo "<td>".$product['vendorPhone']."</td>";
    echo "<td><input type='number' value='' min='1' max='". $product['prodQty'] ."' class='buying-qty' name='prodQty_$counter'/>";    
    echo "</tr>";
    echo "<input type='hidden' value='". $product['vedorId'] ."' name='vendorId_$counter' />";
    echo "<input type='hidden' value='". date("Y-m-d")."' name='purchaseDate_$counter' '>";   
    $counter++;
    echo $counter;
}


?>
      
    
    </tbody>
  </table>
  <input id="buyNow" type='submit' class='buy-now btn btn-success' name='buyProduct' value='Buy Now' disabled>    
  </form>
</div>


<?php include_once "footer.php";?>
