/* 
 * FruitCom Project for Net Energy
 * Author: Dave Jookhuu
 * Sep 17, 2017 
 */

$(document).ready(function() {   
    // on mouse leave
    $(".buying-qty").on("blur", function() {

        /* Getting min & max quantity available for the selected item */ 
        $minVal = parseInt($(this).attr("min"));
        $maxVal = parseInt($(this).attr("max"));
        
         /* For testing */
        console.log("Min value available " + $minVal +" "+ "Max value available " +$maxVal);        
        console.log("User input " + $(this).val());
        $userInput = $(this).val();

        if( parseInt($userInput) > 0  && $userInput !== "" )
        {            
            
            //$minVal = $(this).attr("min");
            //$maxVal = $(this).attr("max");

            if(parseInt($userInput) >= $minVal && parseInt($userInput) <= $maxVal)
            {
                $productPrice = parseFloat($(this).parents('td').siblings().find('input.prodPrice').attr("value"));
                $totalPrice = $(this).parents('td').siblings().find('input.totalPrice').val(($productPrice * parseInt($userInput)).toFixed(2))
                console.log($productPrice);
                $("#buyNow").prop("disabled", false);
            }
            else
            {
                $("#buyNow").prop("disabled", true);
                $(this).val("");
                alert("Quantity must be more than 0 and less than " + $maxVal +"!");
                $(this).parents('td').siblings().find('input.totalPrice').val("");
            }
        }
        else
        {
            $(this).val("");  
            $("#buyNow").prop("disabled", true);
            $(this).parents('td').siblings().find('input.totalPrice').val("");
        }
        
    });

});

/**
* Form Validation Start
*
*/



