<?php 
    session_start();
    include_once "/database/db.class.php";
    include_once "/data/vendor.class.php";
    include_once "/data/login.class.php";    
    include_once "header.php";
    
    if(isset($_SESSION["invalidUser"]) && !empty($_SESSION["invalidUser"]))
    {
        $invalidUser = $_SESSION["invalidUser"];
    }
    else
    {
        session_destroy();
    }
    
    if(isset($_REQUEST['submit']))
    {   
        // Checking if user inputs are not empty, null
        if((!empty($_REQUEST["accountType"]) && isset($_REQUEST["accountType"])) && (!empty($_REQUEST["email"]) && isset($_REQUEST["email"])) && (!empty($_REQUEST["pass"]) && isset($_REQUEST["pass"])))
        {
            // Removing white space
            $accountType = trim($_REQUEST["accountType"]);
            $userEmail = trim($_REQUEST["email"]);
            $userPass = trim($_REQUEST["pass"]);
            
            $login = new Login();            
            $user = $login->userLogin($accountType, $userEmail, $userPass);            
        }
        else
        {
            // Error message
            echo "<h6 class='col-md-4 col-md-offset-4 required'>Username/Password *<h6>";
        }
    }   
    
?>
<div id="login">
    <div class="row">
    <div class="col-md-4 col-md-offset-4">
        <form method="POST" action="">
            Account Type:<select class="form-control" name="accountType" >
                    <option value="">Please choose account type</option>
                    <option value="customer">Buyer</option>
                    <option value="vendor">Seller</option>                    
                  </select><br />
            User Email: <input class="form-control" type="email" name="email" value=""/><br /> 
            User Pass: <input class="form-control" type="password" name="pass" />
            <a href="registration.php">Click here to register</a><br /><br />
            <?php 
            if(!empty($invalidUser) && isset($invalidUser))
            {   
                echo "<div class='alert alert-danger'>". $invalidUser ."</div>";
            } 
            ?>
            <input class="btn" type="submit" value="Login" name="submit" id="submit" onclick="return formValidation();" />
        </form>        
        
        
    </div>
        
    </div>
</div>

<?php
    include_once("footer.php");
?>
