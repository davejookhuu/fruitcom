<?php
    session_start();
    include_once "database/db.class.php";
    include_once "data/vendor.class.php";
    include_once "data/register.class.php";    
    include_once "header.php";
    
    // BuyerUser exists message
    if(isset($_SESSION["buyerExists"]) && !empty($_SESSION["buyerExists"]))
    {
        $buyerExists = $_SESSION["buyerExists"];
    }
    else
    {
        //session_destroy();
    }
    // SellerUser exists message
    if(isset($_SESSION["sellerExists"]) && !empty($_SESSION["sellerExists"]))
    {
        $sellerExists = $_SESSION["sellerExists"];
    }
    else
    {
        //session_destroy();
    }
    
    if(isset($_REQUEST['buyerRegister']) || isset($_REQUEST['sellerRegister']))
    {   
        $requiredField = "";
        $register = new Register();
        if(isset($_REQUEST['buyerRegister']))
        {
            if( (isset($_REQUEST["buyerFirstName"]) && !empty($_REQUEST["buyerFirstName"])) && 
                (isset($_REQUEST["buyerLastName"]) && !empty($_REQUEST["buyerLastName"])) && 
                (isset($_REQUEST["buyerEmail"]) && !empty($_REQUEST["buyerEmail"])) && 
                (isset($_REQUEST["buyerPhone"]) && !empty($_REQUEST["buyerPhone"])) &&
                (isset($_REQUEST["buyerPass"]) && !empty($_REQUEST["buyerPass"])) )
            {
                $buyerFirstName = trim($_REQUEST["buyerFirstName"]);
                $buyerLastName = trim($_REQUEST["buyerLastName"]);
                $buyerEmail = trim($_REQUEST["buyerEmail"]);
                $buyerPhone = trim($_REQUEST["buyerPhone"]);
                $buyerPass = trim($_REQUEST["buyerPass"]);                
                $register->registerBuyer($buyerFirstName, $buyerLastName, $buyerEmail, $buyerPhone, $buyerPass);                
            }
            else
            {
                echo "<h6 class='col-md-4 col-md-offset-4 required'>All Required Fields *<h6>";
                //return false;
                 // Error message
            }   
        }
        elseif(isset($_REQUEST['sellerRegister']))
        {
            if( (isset($_REQUEST["sellerName"]) && !empty($_REQUEST["sellerName"])) && 
                (isset($_REQUEST["sellerEmail"]) && !empty($_REQUEST["sellerEmail"])) && 
                (isset($_REQUEST["sellerPhone"]) && !empty($_REQUEST["sellerPhone"])) && 
                (isset($_REQUEST["sellerPass"]) && !empty($_REQUEST["sellerPass"])) )
                    
            {                
                $sellerName = trim($_REQUEST["sellerName"]);
                $sellerEmail = trim($_REQUEST["sellerEmail"]);
                $sellerPhone = trim($_REQUEST["sellerPhone"]);
                $sellerPass = trim($_REQUEST["sellerPass"]);
                $register->registerSeller($sellerName, $sellerEmail, $sellerPhone, $sellerPass);
            }
            else
            {
                echo $requiredField = "required";
                
                //return false;
                 // Error message
            }           
        }        
    }
if (isset($_REQUEST['buyerEmail']))
{echo $_REQUEST['buyerEmail']; }
?>
<script type="text/javascript">
$(document).ready(function() 
{
    $url = location.search;
    $active = $url.split('=');
    console.log($active);
    if($active[1] == "buyer")
    {
        $("#seller").removeClass("active").removeClass("in");
        $("#buyer").addClass("active").addClass("in");
    }
    else if($active[1] == "seller")
    {
        $("#seller").addClass("active").addClass("in");
        $("#buyer").removeClass("active").removeClass("in");
        $("ul.nav.nav-tabs li:nth-child(2)").addClass("active");
        $("ul.nav.nav-tabs li:nth-child(1)").removeClass("active");
    }
    else
    {
        $("#buyer").addClass("active").addClass("in");
    }
});
</script>


    <div class="row justify-content-center">
        <div class="col-md-4 col-md-offset-4">  
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#buyer">Byer</a></li>
              <li class=""><a data-toggle="tab" href="#seller">Seller</a></li>    
            </ul>

            <div class="tab-content">
              <div id="buyer" class="tab-pane fade in active">
                <h3>Welcome Buyer</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <form method="POST" action="">            
                    Username/Buyer Email: <input class="form-control" type="text" name="buyerEmail" value="<?php echo isset($_REQUEST['buyerRegister']) ? "Hello" : '' ?>"/><br />
                    Buyer First Name: <input class="form-control" type="text" name="buyerFirstName"/><br />
                    Buyer Last Name: <input class="form-control" type="text" name="buyerLastName"/><br />                     
                    Buyer Phone: <input class="form-control" type="text" name="buyerPhone"/><br /> 
                    Password: <input class="form-control" type="password" name="buyerPass" /><br />
                    Repeat Pass: <input id="buyerRepeatPass" class="form-control" type="text" /><br />
                    <?php 
                    if(!empty($buyerExists) && isset($buyerExists))
                    {   
                        echo "<div class='alert alert-danger'>$buyerExists</div>";
                    } 
                    ?>
                    <input class="btn" type="submit" value="Login" name="buyerRegister" />
                </form>
              </div>
              <div id="seller" class="tab-pane fade">
                <h3>Welcome Seller</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">            
                    Username/Seller Email: <input class="form-control" type="text" name="sellerEmail"/><br />   
                    Seller Name: <input class="form-control" type="text" name="sellerName"/><br />                                          
                    Seller Phone: <input class="form-control" type="text" name="sellerPhone"/><br /> 
                    Password: <input class="form-control" type="password" name="sellerPass" /><br />
                    Repeat Pass: <input id="sellerRepeatPass" class="form-control" type="text" /><br />
                    <?php 
                    if(!empty($sellerExists) && isset($sellerExists))
                    {   
                        echo "<div class='alert alert-danger'>$sellerExists</div>";
                    } 
                    ?>
                    <input class="btn" type="submit" value="Login" name="sellerRegister" />
                </form>
              </div>   
            </div>
        </div>     
    </div>
</div>


<?php
    include_once("footer.php");
?>


