<?php
session_start();
include_once "/database/db.class.php";
include_once "/data/vendor.class.php";
include_once "/data/product.class.php";
include_once "/data/login.class.php";    
include_once "header.php";

$logOut = new Login();
$db = new DB();
echo "<div class='container'>";
if(isset($_SESSION["loggedUser"]) && !empty($_SESSION["loggedUser"]))
{
    if($_SESSION["customerType"] === "vendor")
    {
        echo "<h6>Welcome <strong>". $_SESSION["loggedUser"]. "</strong>!</h6>";
        if(isset($_SESSION["loggedInVendorId"]))
        {
            $loggedInVendorId = $_SESSION["loggedInVendorId"];
        }
        if(isset($_REQUEST["logOut"]))
        {
            $logOut->logOut();
        }
    }
    else
    {
        header("Location: cust-admin.php");
    }
}
else    
{    
    $logOut->logOut();
}

if(isset($_REQUEST["addProduct"]) || isset($_REQUEST["updateProduct"]) || isset($_REQUEST["deleteProduct"]))
{
    $prodId = trim($_REQUEST["prodId"]);
    $prodName = trim($_REQUEST["prodName"]);
    $prodPrice = trim($_REQUEST["prodPrice"]);
    $prodQty = trim($_REQUEST["prodQty"]);
    
    if( (!empty($prodName) && isset($prodName)) && 
        (!empty($prodPrice) && isset($prodPrice)) && 
        (!empty($prodQty) && isset($prodQty)) )
    {
        $prod = new Product($prodId, $prodName, $prodPrice, $prodQty, $loggedInVendorId);
        if(isset($_REQUEST["addProduct"]))
        {            
            $prod->addProduct();
        }
        elseif(isset($_REQUEST["updateProduct"]))
        {
            $prod->updateProduct();
            //$db->execute("UPDATE productavailable SET prodName = '$prodName', prodPrice = '" . $this->getProdPrice(). "', prodQty = '" . $this->getProdQty() . "', vendorId = '" . $this->getVendorId() . "'  WHERE id=" . $this->getProdId());
        }
        elseif(isset($_REQUEST["deleteProduct"]))
        {
            //$prod->deleteProduct();
            //$db->execute("DELETE FROM productavailable WHERE prodId = 4");
            $sql = "DELETE FROM productavailable WHERE id=$prodId";

            if (mysqli_query($db->connect(), $sql)) {
                echo "Record deleted successfully";
            } else {
                echo "Error deleting record: " . $db->connect()->error;
            }
        }
        
    }
    else
    {
        // All fields required message
        
    }
    
    // Getting last updated time
    
}
?>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
    <input class="btn btn-info" type="submit" name="logOut" value="Logout" />
    
</form>
<h2>Products For Sell</h2>
  <p>Lorem ipsum dolor sit amet, odio dicta mauris in commodo, tellus ut leo est, hac viverra ac amet ridiculus, a sed egestas donec vitae fusce, risus pede id ut non. Odio phasellus ac est, facilisis nec labore praesent, vitae semper urna, id venenatis ipsum leo felis nam orci. Curabitur egestas suscipit enim mauris morbi, consectetuer et justo lorem, at nulla sed bibendum in vitae. Mollis elit, quis mauris. Cras rerum mattis ligula mus, vestibulum dolor lectus, aliquam lorem enim, duis ut nibh enim cursus. Nam habitasse, natus vel nisl quis vel quam.</p>            
  <table class="table table-striped">
    <thead>        
      <tr>
        <th>Product Name</th>
        <th>Product Price</th>
        <th>Product Quantity</th>
        <th>Date Added</th>   
      </tr>
    </thead>
    <tbody>         
        <?php 
       
        $productsQuery = "SELECT * FROM productavailable WHERE vendorId=".intval($loggedInVendorId)."";
        $products = $db->getAll($productsQuery); // select ALL from users		
        $numberOfProducts = count($products); // return the number of lines
        echo "<h6 class='required'>". $numberOfProducts ." products available to sell.<h6><br />";
        foreach($products as $product) { // display the list
            //`prodName`, `prodPrice`, `prodQty`, `vendorId`
            echo "<tr>";
            echo "<td>".ucwords($product['prodName'])."</td>";
            echo "<td>CAD ".$product['prodPrice']."</td>";
            echo "<td>".$product['prodQty']."</td>";
            echo "<td>". date("Y-m-d")."</td>"; 
            echo "</tr>";
        }
        ?>
    </tbody>
  </table>
</form>

<div class="col-md-6">  

<form method="GET" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">    
    <div class="col-md-6">            
    <select class="form-control" name="productId" onchange="this.form.submit();">
    <option value="">Update/Delete Product</option>
        <?php 
        $prodNames = "SELECT id, prodName FROM productavailable WHERE vendorId=".intval($loggedInVendorId)."";
        $names = $db->getAll($prodNames); // select ALL from users	
        foreach ($names as $name)
        {
            echo "<option value=". $name['id'] .">". $name['prodName'] ."</option>";
        }
        ?>       
    </select>
    </div>
</form>

</div>

<br/>
<br/>
<br/>
<?php 
if(isset($_GET["productId"]))
{   
    $prodNames = "SELECT * FROM productavailable WHERE id=".$_GET["productId"];
    $names = $db->getOne($prodNames); // select ALL from users	
    
    ?>
<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">       
    <div class="col-md-6">            
    Product ID: <input class="form-control" type="text" name="prodId" value="<?php print($names['id'])?>" readonly/><br />
    Product Name: <input class="form-control" type="text" name="prodName" value="<?php echo $names['prodName']?>" /><br /> 
    Product Price: <input class="form-control" type="text" name="prodPrice" value="<?php echo $names['prodPrice']?>" /><br /> 
    Product Qty: <input class="form-control" type="text" name="prodQty" value="<?php echo $names['prodQty']?>" /><br />
    <input class="btn btn-info" type="submit" value="Add New Product" name="addNew" />    
    <input class="btn btn-success" type="submit" value="Update Product" name="updateProduct" />                
    <input class="btn btn-danger" type="submit" value="Delete Product" name="deleteProduct" onclick="return confirm('Are you sure to delete?')" />
    </div>
</form>
<?php
}
else if(isset($_GET["addNew"]))
{?>
<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">       
    <div class="col-md-6">            
    Product ID: <input class="form-control" type="text" name="prodId" value="" readonly/><br />
    Product Name: <input class="form-control" type="text" name="prodName" value="" /><br /> 
    Product Price: <input class="form-control" type="text" name="prodPrice" value="" /><br /> 
    Product Qty: <input class="form-control" type="text" name="prodQty" value="" /><br /> 
    <input class="btn btn-info" type="submit" value="Add New Product" name="addProduct" />
    </div>
</form>
<?php
} 
else
{?>
<form method="POST" action="vend-admin.php">       
    <div class="col-md-6">            
    Product ID: <input class="form-control" type="text" name="prodId" value="" readonly/><br />
    Product Name: <input class="form-control" type="text" name="prodName" value="" /><br /> 
    Product Price: <input class="form-control" type="text" name="prodPrice" value="" /><br /> 
    Product Qty: <input class="form-control" type="text" name="prodQty" value="" /><br /> 
    <?php 
        if(isset($_GET["productExists"]))
        {   
            echo "<div class='alert alert-danger'>Product Exists!</div>";
        } 
    ?>
    <input class="btn btn-info" type="submit" value="Add New Product" name="addProduct" />
    </div>
</form>
<?php }?>
<input class="btn btn-info" type="submit" value="Download My Products as CSV File" name="downloadCSV" />
</div>
<?php
    include_once("footer.php");
?>
